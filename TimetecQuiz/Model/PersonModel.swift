//
//  PersonModel.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import Foundation
import Combine

// MARK: - Enum

enum userType: String {

  case CEO = "Chief Executive Officer"
  case COO = "Chief Operating Officer"
  case Admin = "Administrator"
  case Leader = "Leader"
  case Member = "Member"
}

// MARK: - func
func userPosition(for user: String) -> Int {
    
    let users = userType(rawValue: user) ?? .none
    
    switch users {
    case .CEO:
      return 1
    case .COO:
      return 2
    case .Admin:
      return 3
    case .Leader:
      return 4
    case .Member:
      return 5
    case .none: //for invalid data
        return 6
    }
}

func getUser(){
    userData.append(user1)
    userData.append(user2)
    userData.append(user3)
    userData.append(user4)
    userData.append(user5)
    userData.append(user6)
    userData.append(user7)
    userData.append(user8)
     
    userData = userData.sorted(by: { userPosition(for: $0.userType.capitalized) < userPosition(for: $1.userType.capitalized) })
}

func determineDarkMode() {
    let darkMode = UserDefaults.standard.bool(forKey: "darkMode")
    SceneDelegate.shared?.window!.overrideUserInterfaceStyle = darkMode ? .dark : .light
}

// MARK: - Structure
struct PersonModel {
    var userType: String
    var responsibility: String
    var personalInfo: PersonDetailsModel
    
    init(userType: String, responsibility: String, personalInfo: PersonDetailsModel) {
        self.userType = userType
        self.responsibility = responsibility
        self.personalInfo = personalInfo
    }
}

struct PersonDetailsModel {
    let id: String
    var name: String
    var age: Int
    var gender: String
    
    init(id: String, name: String, age: Int, gender: String) {
        self.id = id
        self.name = name
        self.age = age
        self.gender = gender
    }
}

struct ToggleModel {
    var isDark: Bool = UserDefaults.standard.bool(forKey: "darkMode") {
        didSet {
            SceneDelegate.shared?.window!.overrideUserInterfaceStyle = isDark ? .dark : .light
            UserDefaults.standard.set(isDark ? true : false, forKey: "darkMode")
        }
    }
}
// MARK: - Class
