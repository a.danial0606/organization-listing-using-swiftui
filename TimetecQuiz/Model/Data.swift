//
//  Data.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import Foundation


// TODO: create a new class store the list below, create a static function to get the list. use enumeration to handle userType & gender for extra points.
let user1 = PersonModel(userType: "Member",
                        responsibility: "Participate in meetings and shares knowledge.",
                        personalInfo: PersonDetailsModel(id: "1",
                                                         name: "Gem Tang",
                                                         age: 28,
                                                         gender: "Female 🙋‍♀️"))
let user2 = PersonModel(userType: "Member",
                        responsibility: "Update progress.",
                        personalInfo: PersonDetailsModel(id: "2",
                                                         name: "Nicholas Chow",
                                                         age: 24,
                                                         gender: "Male 🙋‍♂️"))
let user3 = PersonModel(userType: "Chief Operating Officer",
                        responsibility: "A senior executive tasked with overseeing the day-to-day administrative and operational functions of a business.",
                        personalInfo: PersonDetailsModel(id: "3",
                                                         name: "Barry Chai",
                                                         age: 36,
                                                         gender: "Male 🙋‍♂️"))
let user4 = PersonModel(userType: "Administrator",
                        responsibility: "Oversee and maintain all aspects of a company’s computer infrastructure which includes maintaining networks, servers and security programs and systems.",
                        personalInfo: PersonDetailsModel(id: "4",
                                                         name: "Nicki Minaj",
                                                         age: 26,
                                                         gender: "Female 🙋‍♀️"))
let user5 = PersonModel(userType: "Chief Executive Officer",
                        responsibility: "The highest-ranking executive in a company, whose primary responsibilities include making major corporate decisions, managing the overall operations and resources of a company, acting as the main point of communication between the board of directors (the board) and corporate operations.",
                        personalInfo: PersonDetailsModel(id: "5",
                                                         name: "Mr. Teh",
                                                         age: 50,
                                                         gender: "Male 🙋‍♂️"))
let user6 = PersonModel(userType: "Member", responsibility: "",
                        personalInfo: PersonDetailsModel(id: "6",
                                                         name: "Jason Teow",
                                                         age: 25,
                                                         gender: "Male 🙋‍♂️"))
let user7 = PersonModel(userType: "Leader",
                        responsibility: "A leader is someone who provides direction, instructions and guidance to a group of individuals.",
                        personalInfo: PersonDetailsModel(id: "7",
                                                         name: "Jinny Wong",
                                                         age: 18,
                                                         gender: "Female 🙋‍♀️"))
let user8 = PersonModel(userType: "MEMBER",
                        responsibility: "",
                        personalInfo: PersonDetailsModel(id: "8",
                                                         name: "Kenny Lee",
                                                         age: 35,
                                                         gender: "Male 🙋‍♂️"))
