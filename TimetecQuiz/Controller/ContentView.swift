//
//  ContentView.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    var body: some View {
        NavigationView {
            HomePage()
            .navigationBarTitle(Text("Organization"), displayMode: .inline)
            .navigationBarItems(trailing: DarkModeButton())
        }.navigationViewStyle(StackNavigationViewStyle())
        
    }
}

struct DarkModeButton: View {
    @State var isToggle = ToggleModel()
    
    var body: some View {
        HStack {
            Toggle(isOn: $isToggle.isDark) {
                Text("")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
           ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
               ContentView()
                 .previewDevice(PreviewDevice(rawValue: deviceName))
           }
    }
}
