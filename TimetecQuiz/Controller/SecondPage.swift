//
//  SecondPage.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import SwiftUI

struct SecondPage: View {
    var userData: PersonModel
    
    var body: some View {
        SecondPageRow(userDetails: userData)
    }
}

struct SecondPage_Previews: PreviewProvider {
    static var previews: some View {
        SecondPage(userData: userData[0])
    }
}
