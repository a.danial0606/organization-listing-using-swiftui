//
//  HomePage.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import SwiftUI

//global var to use in entire app
var userData = [PersonModel]()
let deviceWidth = UIScreen.main.bounds.width
let deviceHeight = UIScreen.main.bounds.height

struct HomePage: View {
    
    init() {
       determineDarkMode()
       getUser()
        
       UITableView.appearance().tableFooterView = UIView()
      
        // MARK: - Additional Question
        //TODO: print out the list of name with the condition of age is greater than 27
        //filter the struct array for age > 27 only
        let userGreaterThan27YO = userData.filter{ $0.personalInfo.age > 27 }
        
        userGreaterThan27YO.forEach { (data) in
            print(data.personalInfo.name)
        }
    }
    
    var body: some View {
        List(0..<userData.count) { index in
            NavigationLink(destination: SecondPage(userData: userData[index])){
                HomePageRow(userDetails: userData[index])
            }
        }
    }
}

struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage()
    }
}
