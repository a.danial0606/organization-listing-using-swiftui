//
//  HomePageRow.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import SwiftUI

struct HomePageRow: View {
    let userDetails: PersonModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(userDetails.userType.capitalized)
                .font(.system(size: 17, weight: .semibold, design: .default))
                .padding([.bottom, .leading], 5.0)
       
             HStack(alignment: .top) {
                CircleImage().frame(width: deviceWidth/6, height: deviceWidth/6)
                VStack(alignment: .leading, spacing: 5) {
                    Text(userDetails.personalInfo.name.capitalized)
                        .font(.system(size: 17, weight: .regular, design: .default))
                    Text("Gender: \(userDetails.personalInfo.gender.capitalized)")
                        .font(.system(size: 17, weight: .regular, design: .default))
                    Text("Age: \(userDetails.personalInfo.age)")
                        .font(.system(size: 17, weight: .regular, design: .default))
                    
                    if !userDetails.responsibility.isEmpty{
                        Text("Responsiblity: \(userDetails.responsibility)")
                        .font(.system(size: 17, weight: .regular, design: .default))
                        .lineLimit(nil)
                    }
                }.padding(.trailing, 20.0)
            }.padding(.bottom, 10.0)
        }
    }
}

struct CircleImage: View {
    var body: some View {
      Circle()
        .scaledToFit()
        .clipShape(Circle())
        .overlay(
            Circle().stroke(Color.gray, lineWidth: 2))
        .shadow(radius: 3)
        .foregroundColor(Color(UIColor.hexStringToUIColor(hex: "#4BCDF5")))
    }
}

struct HomePageRow_Previews: PreviewProvider {
    static var previews: some View {
       Group{
        HomePageRow(userDetails: userData[0])
       }.previewLayout(.fixed(width: 300, height: 150))
    }
}
