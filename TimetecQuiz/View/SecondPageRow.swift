//
//  SecondPageRow.swift
//  TimetecQuiz
//
//  Created by Danial Fajar on 28/12/2019.
//  Copyright © 2019 Danial Fajar. All rights reserved.
//

import SwiftUI

struct SecondPageRow: View {
    
    let userDetails: PersonModel
    
    var body: some View {
        VStack(spacing: 0) {
            List {
                PersonalInfoView(userDetails: userDetails)
                AdditionalInfo(userDetails: userDetails)
            }.listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
        }.navigationBarTitle(Text(userDetails.personalInfo.name.capitalized), displayMode: .automatic)
    }
}

struct PersonalInfoView: View {
    let userDetails: PersonModel
    
    var body: some View {
        Section(header:
          Group {
                VStack(alignment: .center){
                    CircleImage().frame(width: deviceWidth/3, height: deviceWidth/3).padding([.top,. bottom], 20)
                }.frame(width: UIScreen.main.bounds.width)
                VStack(alignment: .leading, spacing: 20 ) {
                Text("Personal Info").font(.system(size: 17, weight: .semibold, design: .default)
                ).foregroundColor(.gray)}
          }) {
            Group {
                VStack(alignment: .leading) {
                        Text("Name")
                            .font(.system(size: 17, weight: .semibold, design: .default))
                        Text(userDetails.personalInfo.name.capitalized)
                        .font(.system(size: 17, weight: .regular, design: .default))
                }
                VStack(alignment: .leading) {
                    Text("Age")
                           .font(.system(size: 17, weight: .semibold, design: .default))
                    Text("\(userDetails.personalInfo.age)")
                       .font(.system(size: 17, weight: .regular, design: .default))
                }
                VStack(alignment: .leading) {
                   Text("Gender")
                       .font(.system(size: 17, weight: .semibold, design: .default))
                   Text(userDetails.personalInfo.gender.capitalized)
                   .font(.system(size: 17, weight: .regular, design: .default))
                }
           }.padding([.top, .bottom], 5.0)
        }
    }
}

struct AdditionalInfo: View {
    let userDetails: PersonModel
    
    var body: some View {
        Section(header: Text("Additional Info").font(.system(size: 17, weight: .semibold, design: .default)).foregroundColor(.gray)) {
          Group {
            VStack(alignment: .leading) {
                Text("User Role")
                    .font(.system(size: 17, weight: .semibold, design: .default))
                Text(userDetails.userType.capitalized)
                .font(.system(size: 17, weight: .regular, design: .default))
            }
             if !userDetails.responsibility.isEmpty{
               VStack(alignment: .leading) {
                    Text("Responsibility")
                        .font(.system(size: 17, weight: .semibold, design: .default))
                    Text(userDetails.responsibility)
                        .font(.system(size: 17, weight: .regular, design: .default))
                        .lineLimit(nil)
                }
             }
          }.padding([.top, .bottom], 5.0)
       }
    }
}

struct SecondPageRow_Previews: PreviewProvider {
    static var previews: some View {
        SecondPageRow(userDetails: userData[0])
    }
}
